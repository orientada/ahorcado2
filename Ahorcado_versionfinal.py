import random

class Juego():
    
    def __init__(self, palabras):
        
        self.palabras=palabras    
        self.palabra_elegida=random.choice(palabras)        
        self.vidas=10
        self.miPalabra=["-"]*len(self.palabra_elegida)
        self.letra_correcta=[]
        self.letra_incorrecta=[]
        
    def verificar_letra(self):
        
        while True:
            print ("Esta es la cantidad de letras que tiene la palabra a adivinar")
            print (' ' .join(self.miPalabra))
            letra=input("Ingrese una letra: " ) 
            if letra in self.palabra_elegida:
                self.letra_correcta.append(letra)
                for i in range(len(self.palabra_elegida)):
                  if self.palabra_elegida[i] == letra:
                      self.miPalabra[i] = letra
                if "-" not in self.miPalabra:
                    print("Has adivinado la palabra secreta")
                    break
            else:
                 self.letra_incorrecta.append(letra)
                 self.vidas -= 1
                 
                 if self.vidas == 0:
                    print("Se acabaron las vidas. La palabra a adivinar era:", self.palabra_elegida)
                    break
                
palabras=["cuadrado", "circulo", "triangulo", "rectangulo"]
iniciar=Juego(palabras)
iniciar.verificar_letra()